package com.example.my_google.config;

import com.example.my_google.constant.QueryField;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Analyzer analyzer() {
        return new StandardAnalyzer();
    }

    @Bean
    public QueryParser queryParser(Analyzer analyzer) {
        return new QueryParser(QueryField.CONTENT_FIELD.getName() ,analyzer);
    }
}
