package com.example.my_google.constant;

public enum QueryField {
    URL_FIELD("url"),
    TITLE_FIELD("title"),
    CONTENT_FIELD("content");

    private final String name;

    QueryField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
