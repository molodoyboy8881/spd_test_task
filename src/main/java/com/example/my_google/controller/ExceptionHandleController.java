package com.example.my_google.controller;

import com.example.my_google.exception.IndexException;
import com.example.my_google.exception.SearchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandleController {
    private static final Logger logger =
            LoggerFactory.getLogger(ExceptionHandleController.class.getName());

    @ExceptionHandler(SearchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String searchExceptionHandler(SearchException e, Model model) {
        logger.warn(e.getMessage());

        model.addAttribute("error", e.getMessage());
        return "search_result";
    }

    @ExceptionHandler(IndexException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String indexExceptionHandler(IndexException e, Model model) {
        logger.warn(e.getMessage());

        model.addAttribute("error", e.getMessage());
        return "index";
    }
}
