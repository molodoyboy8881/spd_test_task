package com.example.my_google.controller;

import com.example.my_google.exception.IndexException;
import com.example.my_google.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/index")
public class IndexController {
    private final IndexService indexService;

    @Autowired
    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping
    public String indexPage() {
        return "index";
    }

    @PostMapping
    public String indexUrl(@RequestParam String url,
                           @RequestParam(required = false) Integer indexDepth) throws IndexException {
        indexService.index(url, indexDepth);

        return "redirect:/index";
    }
}
