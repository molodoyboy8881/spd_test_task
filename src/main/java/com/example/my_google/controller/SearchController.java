package com.example.my_google.controller;

import com.example.my_google.domain.SearchResult;
import com.example.my_google.exception.SearchException;
import com.example.my_google.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {
    private final SearchService searcherService;

    @Autowired
    public SearchController(SearchService searcherService) {
        this.searcherService = searcherService;
    }

    @PostMapping
    public String search(@RequestParam String query, Model model) throws SearchException {
        List<SearchResult> searchResults = searcherService.search(query);
        model.addAttribute("searchResults", searchResults);

        return "search_result";
    }
}
