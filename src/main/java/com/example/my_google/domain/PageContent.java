package com.example.my_google.domain;

import org.jsoup.nodes.Document;

import java.util.Objects;

public class PageContent {
    private final String location;
    private final String title;
    private final String content;

    private PageContent(String location, String title, String content) {
        this.location = location;
        this.title = title;
        this.content = content;
    }

    public static PageContent from(Document document) {
        return new PageContent(document.location(), document.title(), document.body().text());
    }

    public String getLocation() {
        return location;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageContent that = (PageContent) o;
        return location.equals(that.location) && title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, title);
    }

    @Override
    public String toString() {
        return "PageContent{" +
                "url='" + location + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
