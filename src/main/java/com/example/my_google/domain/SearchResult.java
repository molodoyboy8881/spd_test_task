package com.example.my_google.domain;

import org.apache.lucene.document.Document;

public class SearchResult {
    private final String url;
    private final String title;

    private SearchResult(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public static SearchResult from(Document document) {
        return new SearchResult(document.get("url"), document.get("title"));
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
