package com.example.my_google.exception;

public class IndexException extends RuntimeException {

    public IndexException(String message) {
        super(message);
    }
}
