package com.example.my_google.exception;

public class ScrapperException extends RuntimeException {

    public ScrapperException(String message) {
        super(message);
    }
}
