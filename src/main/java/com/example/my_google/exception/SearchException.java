package com.example.my_google.exception;

public class SearchException extends RuntimeException {

    public SearchException(String message) {
        super(message);
    }
}
