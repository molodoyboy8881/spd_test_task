package com.example.my_google.repository;

import com.example.my_google.exception.IndexException;
import com.example.my_google.domain.PageContent;
import com.example.my_google.repository.mapper.DocumentMapper;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

@Service
public class IndexRepository {
    private final Analyzer analyzer;
    private final DocumentMapper documentMapper;

    @Autowired
    public IndexRepository(Analyzer analyzer) {
        this.analyzer = analyzer;
        this.documentMapper = new DocumentMapper();
    }

    public void indexPagesContent(Set<PageContent> pagesContent, Path indexDirectory) throws IndexException {
        IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
        indexConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        try (IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexDirectory), indexConfig)) {
            for (PageContent pageContent : pagesContent) {
                Document document = documentMapper.mapToDocument(pageContent);
                indexWriter.addDocument(document);
            }

        } catch (IOException e) {
            throw new IndexException("Index exception: " + e.getMessage());
        }
    }
}
