package com.example.my_google.repository;

import com.example.my_google.exception.SearchException;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

@Repository
public class SearchRepository {
    private final QueryParser queryParser;

    @Value("${search.elements.number}")
    private Integer numberOfResultElements;

    public SearchRepository(QueryParser parser) {
        this.queryParser = parser;
    }

    public List<Document> search(String searchText, Path indexDirectory) throws SearchException {
        try (IndexReader reader = DirectoryReader.open(FSDirectory.open(indexDirectory))) {
            IndexSearcher indexSearcher = new IndexSearcher(reader);
            Query query = queryParser.parse(searchText);
            TopDocs topDocs = indexSearcher.search(query, numberOfResultElements, Sort.RELEVANCE);

            return getSearchDocuments(indexSearcher, topDocs);
        } catch (IOException | ParseException e) {
            throw new SearchException("Cant parse query!");
        }
    }

    private List<Document> getSearchDocuments(IndexSearcher indexSearcher, TopDocs topDocs) throws IOException {
        List<Document> searchResults = new LinkedList<>();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            searchResults.add(doc);
        }

        return searchResults;
    }
}
