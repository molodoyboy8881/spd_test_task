package com.example.my_google.repository.mapper;

import com.example.my_google.domain.PageContent;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexOptions;

import static com.example.my_google.constant.QueryField.*;

public class DocumentMapper {

    public Document mapToDocument(PageContent pageContent) {
        Field url = new Field(URL_FIELD.getName(), pageContent.getLocation(), urlFieldType());
        Field title = new TextField(TITLE_FIELD.getName(), pageContent.getTitle(), Field.Store.YES);
        Field content = new TextField(CONTENT_FIELD.getName(), pageContent.getContent(), Field.Store.NO);

        Document document = new Document();
        document.add(url);
        document.add(title);
        document.add(content);

        return document;
    }

    private FieldType urlFieldType() {
        FieldType urlFieldType = new FieldType();
        urlFieldType.setTokenized(false);
        urlFieldType.setIndexOptions(IndexOptions.NONE);
        urlFieldType.setStored(true);

        return urlFieldType;
    }
}
