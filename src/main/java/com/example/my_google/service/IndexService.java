package com.example.my_google.service;

import com.example.my_google.domain.PageContent;
import com.example.my_google.exception.IndexException;
import com.example.my_google.repository.IndexRepository;
import com.example.my_google.service.validator.IndexValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Set;

@Service
public class IndexService {
    private final IndexValidator indexValidator;
    private final IndexRepository indexRepository;
    private final ScrapperService scrapperService;

    @Value("${scrapping.depth}")
    private int defaultScrapperDepth;

    @Value("${index.directory.path}")
    private String indexDirectoryPath;

    @Autowired
    public IndexService(IndexValidator indexValidator,
                        IndexRepository indexRepository,
                        ScrapperService scrapperService) {
        this.indexValidator = indexValidator;
        this.indexRepository = indexRepository;
        this.scrapperService = scrapperService;
    }

    public void index(String url, Integer indexDepth) throws IndexException {
        indexValidator.validate(url, indexDepth);

        Set<PageContent> pageContents = scrapperService.scrap(url, indexDepth != null ? indexDepth : defaultScrapperDepth);
        Path indexDirectory = Path.of(indexDirectoryPath);
        indexRepository.indexPagesContent(pageContents, indexDirectory);
    }
}
