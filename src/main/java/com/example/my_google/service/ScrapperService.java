package com.example.my_google.service;

import com.example.my_google.domain.PageContent;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.example.my_google.utils.ScrapperUtils.*;

@Service
public class ScrapperService {
    private final Logger logger =
            LoggerFactory.getLogger(ScrapperService.class.getName());

    public Set<PageContent> scrap(String url, int scrapperDepth) {
        Set<PageContent> pageContents = new HashSet<>();
        scrap(url, scrapperDepth, pageContents);

        return pageContents;
    }

    private void scrap(String url, int scrapperDepth, Set<PageContent> pageContents) {
        if (!url.isEmpty()) {
            try {
                Document document = getDocumentByUrl(url);
                PageContent pageContent = PageContent.from(document);
                pageContents.add(pageContent);
                if (scrapperDepth > 0) {
                    for (String href : getAllLinks(document)) {
                        scrap(href, --scrapperDepth, pageContents);
                    }
                }

            } catch (IOException e) {
                logger.warn("Cant connect to url: " + url + e.getMessage());
            }
        }
    }
}
