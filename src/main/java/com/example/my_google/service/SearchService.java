package com.example.my_google.service;

import com.example.my_google.domain.SearchResult;
import com.example.my_google.exception.SearchException;
import com.example.my_google.repository.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchService {
    private final SearchRepository searchRepository;

    @Value("${index.directory.path}")
    private String directoryPath;

    @Autowired
    public SearchService(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    public List<SearchResult> search(String searchText) throws SearchException {
        Path indexDirectory = Path.of(directoryPath);
        return searchRepository.search(searchText, indexDirectory).stream()
                .map(SearchResult::from)
                .collect(Collectors.toList());
    }
}
