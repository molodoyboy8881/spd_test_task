package com.example.my_google.service.validator;

import com.example.my_google.exception.IndexException;
import org.springframework.stereotype.Component;

@Component
public class IndexValidator {
    private static final String VALID_URL_PATTERN =
            "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)";

    public void validate(String url, Integer indexDepth) {
        if (!url.matches(VALID_URL_PATTERN)) {
            throw new IndexException("Invalid url!");
        } else if (!(indexDepth == null || indexDepth >= 0)) {
            throw new IndexException("Invalid index depth!");
        }
    }
}
