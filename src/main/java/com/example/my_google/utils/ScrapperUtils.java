package com.example.my_google.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public final class ScrapperUtils {

    private ScrapperUtils() {}

    public static Document getDocumentByUrl(String url) throws IOException {
        return Jsoup.connect(url)
                .timeout(10 * 1000)
                .followRedirects(true)
                .userAgent("Chrome")
                .ignoreContentType(true)
                .ignoreHttpErrors(true)
                .get();
    }

    public static Set<String> getAllLinks(Document document) {
        Elements elements = document.select("a[href]");
        return elements.stream()
                .map(element -> element.attr("abs:href"))
                .collect(Collectors.toSet());
    }
}
