package com.example.my_google.argumentprovider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IndexValidationCorrectArguments implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of("https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-params/5.7.2", null),
                Arguments.of("https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-params/5.7.2", 1)
        );
    }
}
