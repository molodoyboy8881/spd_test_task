package com.example.my_google.argumentprovider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IndexValidationWrongArguments implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(Arguments.of("invalid url", 1),
                        Arguments.of("https://stackoverflow.com/questions/155436/unit-test-naming-best-practices", -1),
                        Arguments.of("https:/", 2)
        );
    }
}
