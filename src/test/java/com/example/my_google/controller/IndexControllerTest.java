package com.example.my_google.controller;

import com.example.my_google.exception.IndexException;
import com.example.my_google.service.IndexService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(IndexController.class)
public class IndexControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IndexService indexService;
    private LinkedMultiValueMap<String, String> requestParams;

    @BeforeEach
    void setUp() {
        requestParams = new LinkedMultiValueMap<>();
        requestParams.add("url", "string");
        requestParams.add("indexDepth", "1");
    }

    @Test
    void when_correct_param_passed_response_ok() throws Exception {
        mockMvc.perform(
                        post("/index")
                                .params(requestParams)


                ).andExpect(status().is(200))
                .andExpect(view().name("index"));
    }

    @Test
    void when_search_exception_thrown_response_bad_request() throws Exception {
        Mockito.doThrow(IndexException.class).when(indexService).index(anyString(), Mockito.anyInt());
        mockMvc.perform(
                        post("/index")
                                .params(requestParams)


                ).andExpect(status().is(400))
                .andExpect(view().name("index"));
    }
}
