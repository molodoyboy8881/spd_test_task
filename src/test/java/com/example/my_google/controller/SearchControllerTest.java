package com.example.my_google.controller;

import com.example.my_google.exception.SearchException;
import com.example.my_google.service.SearchService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(SearchController.class)
class SearchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchService searcherService;

    @Test
    void when_correct_param_passed_response_ok() throws Exception {
        when(searcherService.search(anyString())).thenReturn(List.of());
        mockMvc.perform(
                post("/search")
                        .param("query", anyString())

                ).andExpect(status().isOk())
                .andExpect(view().name("search_result"))
                .andExpect(model().attribute("searchResult", List.of()));
    }

    @Test
    void when_search_exception_thrown_response_bad_request() throws Exception {
        SearchException exception = Mockito.mock(SearchException.class);
        when(exception.getMessage()).thenReturn("Cant parse query!");
        when(searcherService.search(anyString())).thenThrow(exception);
        mockMvc.perform(
                        post("/search")
                                .param("query", anyString())

                ).andExpect(status().is(400))
                .andExpect(view().name("search_result"))
                .andExpect(model().attribute("error", "Cant parse query!"));
    }
}
