package com.example.my_google.service;

import com.example.my_google.domain.PageContent;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ScrapperService.class})
class ScrapperServiceTest {

    @Autowired
    private ScrapperService scrapperService;

    @Test
    void when_scrapper_depth_zero_content_size_must_be_one() {
        Set<PageContent> pageContents = scrapperService.scrap("https://www.postgresqltutorial.com/", 0);
        assertEquals(1, pageContents.size());
    }

    @Test
    void when_incorrect_url_passed_content_size_must_be_zero() {
        Set<PageContent> pageContents = scrapperService.scrap("incorrect url", 0);
        assertEquals(0, pageContents.size());
    }
}
