package com.example.my_google.service.validator;

import com.example.my_google.argumentprovider.IndexValidationCorrectArguments;
import com.example.my_google.argumentprovider.IndexValidationWrongArguments;
import com.example.my_google.exception.IndexException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.*;

class IndexValidatorTest {

    private IndexValidator indexValidator;

    @BeforeEach
    void setUp() {
        indexValidator = new IndexValidator();
    }

    @ParameterizedTest
    @ArgumentsSource(IndexValidationCorrectArguments.class)
    void when_correct_ags_passed_must_throw_nothing(String url, Integer indexDepth) {
        assertDoesNotThrow(() -> indexValidator.validate(url, indexDepth));
    }

    @ParameterizedTest
    @ArgumentsSource(IndexValidationWrongArguments.class)
    void when_incorrect_ags_passed_must_throw_exception(String url, Integer indexDepth) {
        assertThrows(IndexException.class, () -> indexValidator.validate(url, indexDepth));
    }
}
